# README

Install RVM  
Install Rails (this is Rails 5 running on top of Ruby 2.3.1)  
Install NodeJS: sudo apt-get install -y nodejs  

# Rails dev server:  
rake db:drop  
rake db:create  
rake db:migrate  
rake db:seed  
rails s  
  
With Postman or other REST client:  

HTTP POST to create event:  
localhost:3000/events.json (string_entry as optional BODY param)  

HTTP GET to list 5 events from 'http://localhost:3000/events.json':  
localhost:3000/events.json?hostname=http://localhost:3000/events.json&number=5  

HTTP GET to list 3 events from 'localhost' domain:  
localhost:3000/events.json?host=http://localhost:3000/events.json&number=3  

# Rails console:
rails c  
Event.all.as_json # to see what was created  

# To run tests:

rails db:migrate RAILS_ENV=test  
rspec  

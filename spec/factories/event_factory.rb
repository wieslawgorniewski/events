FactoryGirl.define do
  factory :event do
    sequence :string_entry do |n|
      "Very interesting entry no #{n}."
    end
    hostname 'http://www.cnn.com'
  end
end
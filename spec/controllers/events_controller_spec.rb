require 'rails_helper'

RSpec.describe EventsController, :type => :controller do
  let!(:event_01) { FactoryGirl.create(:event, hostname: 'http://www.cnn.com?interesting_param=123') }
  let!(:event_02) { FactoryGirl.create(:event, hostname: 'http://www.cnn.com') }

  describe 'GET #index' do
    it 'responds with http 200' do
      get :index, format: :json
      expect(response).to have_http_status(200)
    end

    it 'responds with empty list if no params are given' do
      get :index, format: :json
      expect(response.body).to eq([].to_json)
    end

    it 'responds with json list if correct params are given' do
      get :index, params: { host: 'www.cnn.com', number: 2 }, format: :json
      expect(response.body).to eq([event_02, event_01].to_json)
    end

    it 'calls org method if "hostname" param is given' do
      expect(Event).to receive(:org).with('http://www.cnn.com', 2)
      get :index, params: { hostname: 'http://www.cnn.com', number: 2 }, format: :json
    end

    it 'calls org method with number 1 if no number is given' do
      expect(Event).to receive(:org).with('http://www.cnn.com', 1)
      get :index, params: { hostname: 'http://www.cnn.com' }, format: :json
    end

    it 'calls org method if "host" param is given' do
      expect(Event).to receive(:get_by_host).with('www.cnn.com', 2)
      get :index, params: { host: 'www.cnn.com', number: 2 }, format: :json
    end

    it 'calls get_by_host with number 1 if no number is given' do
      expect(Event).to receive(:get_by_host).with('www.cnn.com', 1)
      get :index, params: { host: 'www.cnn.com' }, format: :json
    end
  end

  describe '#POST create' do
    it 'responds with http 204 if event created' do
      post :create, params: { string_entry: 'Very interesting string entry.' }, format: :json
      expect(response).to have_http_status(201)
    end

    it 'responds with json event if event created' do
      post :create, params: { string_entry: 'Very interesting string entry.' }, format: :json
      expect(response.body).to eq(Event.last.to_json)
    end

    it 'sets params correctly' do
      post :create, params: { string_entry: 'Very interesting string entry.' }, format: :json
      created_event = Event.last
      expect(Event.all.count).to eq(3)
      expect(created_event.string_entry).to eq('Very interesting string entry.')
      expect(created_event.hostname).to eq('http://test.host/events')
    end

    it 'responds with 400 if save not happened' do
      allow_any_instance_of(Event).to receive(:save).and_return(false)
      post :create, params: { string_entry: 'Very interesting string entry.' }, format: :json
      expect(response).to have_http_status(400)
    end

    it 'does not create event is save incorrect' do
      allow_any_instance_of(Event).to receive(:save).and_return(false)
      post :create, params: { string_entry: 'Very interesting string entry.' }, format: :json
      expect(Event.all.count).to eq(2)
    end
  end
end

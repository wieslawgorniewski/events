require 'rails_helper'

RSpec.describe Event, :type => :model do
  let!(:event_01) { FactoryGirl.create(:event, hostname: 'http://www.cnn.com/opinions') }
  let!(:event_02) { FactoryGirl.create(:event, hostname: 'http://www.cnn.com') }
  let!(:event_03) { FactoryGirl.create(:event, hostname: 'http://www.cnn.com') }

  describe '#org' do
    [1, 2].each do |n|
      it "gets last #{n} events for org" do
        events = Event.org('http://www.cnn.com', n)
        expect(events.count).to eq(n)
      end
    end

    it 'filters events by hostname' do
      events_cnn = Event.org('http://www.cnn.com', 3)
      event_cnn_opinions = Event.org('http://www.cnn.com/opinions', 3)
      event_cnn_blah = Event.org('http://www.cnn.com/blah', 3)
      expect(events_cnn.count).to eq(2)
      expect(event_cnn_opinions.count).to eq(1)
      expect(event_cnn_blah.count).to eq(0)
    end

    it 'sorts from newest to the oldest' do
      events_cnn = Event.org('http://www.cnn.com', 3)
      expect(events_cnn).to eq([event_03, event_02])
    end

    it 'provides default argument for number of events' do
      events_cnn = Event.org('http://www.cnn.com', 1)
      expect(events_cnn.count).to eq(1)
    end
  end

  describe '#get_by_host' do
    let!(:event_04) { FactoryGirl.create(:event, hostname: 'http://www.bbc.co.uk?redirect=http://www.cnn.com') } # this should not be in the return query

    it 'returns only origins with specified host in hostname' do
      expect(Event.get_by_host('www.cnn.com', 4).count).to eq(3)
      expect(Event.get_by_host('www.bbc.co.uk', 4).count).to eq(1)
    end

    it 'sorts from newest to oldest' do
      expect(Event.get_by_host('www.cnn.com', 4)).to eq([event_03, event_02, event_01])
    end

    it 'provides default argument for number of events' do
      expect(Event.get_by_host('www.cnn.com').count).to eq(1)
    end
  end
end
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


data = [
    {
        hostname: 'http://www.wp.pl',
        string_entry: 'Information Portal.'
    },
    {
        hostname: 'http://cnn.com',
        string_entry: 'Another information portal.'
    },
    {
        hostname: 'https://www.linkedin.com/in/wieslaw-gorniewski',
        string_entry: 'Creator of this app.'
    }
]

data.each do |entry|
  Event.create(hostname: entry[:hostname], string_entry: entry[:string_entry])
end

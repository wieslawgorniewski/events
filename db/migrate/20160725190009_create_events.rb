class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :string_entry
      t.string :hostname

      t.timestamps
    end
  end
end

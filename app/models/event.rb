class Event < ApplicationRecord
  scope :org, ->(org = '', num = 1) { where(hostname: org).order(created_at: :desc).last(num) }

  def self.get_by_host(host, num = 1)
    events = where('hostname LIKE ?', "%#{host}%").order(created_at: :desc)
    events.select { |event| event && URI.parse(event.hostname).host == host }.last(num)
  end
end

class EventsController < ActionController::Base
  before_action :set_number, only: [:index]
  before_action :get_events

  def index
    respond_to do |format|
      format.json { render json: @events.to_json, status: 200 }
    end
  end

  def create
    @event = Event.new(string_entry: params[:string_entry], hostname: request.original_url)
    if @event.save
      respond_to do |format|
        format.json { render json: @event.to_json, status: 201 }
      end
    else
      respond_to do |format|
        format.json { render body: nil, status: 400 }
      end
    end
  end

  private

  def set_number
    @number = params[:number] || 1
  end

  def get_events
    @events = []
    if params[:hostname].present?
      @events = Event.org(params[:hostname], @number.to_i)
    elsif params[:host].present?
      @events = Event.get_by_host(params[:host], @number.to_i)
    end
  end
end
